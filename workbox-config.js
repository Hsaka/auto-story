module.exports = {
  "globDirectory": "public/",
  "globPatterns": [
    "**/*.{js,m4a,ogg,jpg,png,xml,json,css,html}"
  ],
  "swDest": "public\\sw.js"
};