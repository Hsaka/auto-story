import Utils from 'utils/utils';
import UIHelper from 'utils/uiHelper';
import TextInput from 'utils/TextInput.js';
import Categories from 'utils/categories';

export default class DoodleMakerScene extends Phaser.Scene {
  constructor() {
    super('doodlemaker');
  }

  init(data) {
    this.initData = data;
  }

  setup() {
    // this.drawSnd = this.sound.add('draw');
    // this.drawSnd.setLoop(true);
    // this.drawSnd.setVolume(0.1);

    this.hsv = Phaser.Display.Color.HSVColorWheel();
    var i = 0;

    this.lastPx = -1;
    this.lastPy = -1;
    this.startX = -1;
    this.isDrawing = false;
    this.canDraw = true;
    this.drawTime = 0;
    this.simplification = 2;
    this.strokes = [];
    this.points = [];
    this.previewContainer = undefined;
    this.doodleName = '';

    this.screenGroup = this.add.container(0, 0);

    this.bg = this.add.image(0, 0, 'paperbg');
    this.bg.setOrigin(0, 0);
    this.screenGroup.add(this.bg);

    this.titleText = this.add.bitmapText(
      Utils.GlobalSettings.width / 2,
      10,
      'handwriting',
      'Doodle Maker',
      100
    );
    this.titleText.setTint(0x000000).setOrigin(0.5, 0).setCenterAlign();

    this.subtitleText = this.add.bitmapText(
      Utils.GlobalSettings.width / 2,
      130,
      'handwriting',
      'Create new doodles that you can add to your stories!',
      40
    );
    this.subtitleText.setTint(0x000000).setOrigin(0.5, 0).setCenterAlign();

    this.pad = this.add.renderTexture(
      Utils.GlobalSettings.width / 2 - 256,
      Utils.GlobalSettings.height / 2 - 256,
      512,
      512
    );
    this.pad.setOrigin(0, 0);
    this.screenGroup.add(this.pad);

    this.graphics = this.make.graphics();
    this.pad.drawFrame('atlas', 'pad');

    this.drawingCanvas = this.add.rexCanvas(0, 0, 256, 256).setOrigin(0);
    this.drawingCanvas.setAlpha(0);
    this.rc = rough.canvas(this.drawingCanvas.getCanvas());

    this.exitButton = UIHelper.createSystemButton(
      this,
      10,
      10,
      'systembuttons30',
      'systembuttons31'
    );
    this.exitButton.setOrigin(0, 0);
    this.exitButton.setTint(0x000000);
    this.exitButton.on('pointerup', (pointer) => {
      this.scene.start('menu', { from: 'main' });
    });

    this.saveButton = UIHelper.createSystemButton(
      this,
      70,
      10,
      'systembuttons28',
      'systembuttons29'
    );
    this.saveButton.setOrigin(0, 0);
    this.saveButton.setTint(0x000000);
    this.saveButton.on('pointerup', (pointer) => {
      this.errorText.visible = false;
      this.errorText.setText('');

      if (this.doodleName.length > 0) {
        if (this.strokes.length > 0) {
          var safeName = this.doodleName.trim();
          safeName = safeName.toLowerCase();
          safeName = safeName.replace(/[^a-z0-9]/gi, '');

          var doodleData = {
            word: safeName,
            countrycode: 'US',
            timestamp: this.getFormattedTime(),
            recognized: true,
            key_id: Phaser.Math.RND.uuid(),
            drawing: this.strokes,
          };

          if (!Utils.SavedSettings.customDoodles[safeName]) {
            Utils.SavedSettings.customDoodles[safeName] = [doodleData];
          } else {
            Utils.SavedSettings.customDoodles[safeName].push(doodleData);
          }

          Categories.AllCategoriesDictionary[safeName] = safeName;

          Utils.save();

          this.scene.start('menu', { from: 'main' });
        } else {
          this.errorText.visible = true;
          this.errorText.setText('Please draw something!');
        }
      } else {
        this.errorText.visible = true;
        this.errorText.setText('Please enter a name!');
      }
    });

    this.eraseButton = UIHelper.createSystemButton(
      this,
      this.pad.x - 70,
      this.pad.y,
      'systembuttons24',
      'systembuttons25'
    );
    this.eraseButton.setOrigin(0, 0);
    this.eraseButton.setTint(0x000000);
    this.eraseButton.on('pointerup', (pointer) => {
      this.strokes = [];
      this.points = [];
      this.lastPx = -1;
      this.lastPy = -1;
      this.startX = -1;
      this.drawTime = 0;
      this.graphics.clear();
      this.pad.clear();
      this.pad.drawFrame('atlas', 'pad');
      this.previewContainer.visible = false;
    });

    this.nameLabel = this.add.bitmapText(
      this.pad.x + 20,
      this.pad.y + this.pad.height + 30,
      'handwriting',
      'Name: ',
      50
    );
    this.nameLabel.setTint(0x000000).setOrigin(0.5, 0).setCenterAlign();

    this.textField = this.add.textInput('handwriting', {
      x: Utils.GlobalSettings.width / 2,
      y: this.pad.y + this.pad.height + 50,
      width: this.pad.width - 130,
      align: 1,
      size: 50,
      placeholder: '',
      maxlength: 16,
      backgroundAlpha: 1,
    });

    this.textField.on('onchange', (value) => {
      this.doodleName = value;
    });

    this.errorText = this.add.bitmapText(
      Utils.GlobalSettings.width / 2,
      this.pad.y + this.pad.height + 100,
      'handwriting',
      '',
      40
    );
    this.errorText.visible = false;
    this.errorText.setTint(0xff4444).setOrigin(0.5, 0).setCenterAlign();

    this.input.on(
      'pointerup',
      (pointer) => {
        //this.stopDraw();
        //console.log(this.points);
        if (this.lastPx !== -1 && this.lastPy !== -1) {
          if (this.points.length > 0) {
            this.strokes.push(this.points);
            if (!this.previewContainer) {
              this.previewContainer = this.createDrawing(
                'preview-doodle-autostory',
                this.strokes
              );
            } else {
              this.previewContainer.visible = true;
              this.generateTextures('preview-doodle-autostory', this.strokes);
            }
          }
        }

        this.points = [];
        this.lastPx = -1;
        this.lastPy = -1;
        this.startX = -1;
        this.drawTime = 0;
      },
      this
    );

    this.input.on(
      'pointermove',
      (pointer) => {
        if (pointer.isDown) {
          var px = pointer.x;
          var py = pointer.y;

          if (
            px >= this.pad.x &&
            px <= this.pad.x + this.pad.width &&
            py >= this.pad.y &&
            py <= this.pad.y + this.pad.height &&
            this.canDraw
          ) {
            // if (!Utils.SavedSettings.muted) {
            //   if (!this.drawSnd.isPlaying) {
            //     this.drawSnd.play();
            //   }
            // }

            px -= Utils.GlobalSettings.width / 2 - this.pad.width / 2;

            this.isDrawing = true;
            this.drawTime++;

            if (this.lastPx !== -1 && this.lastPy !== -1) {
              this.graphics.lineStyle(8, this.hsv[i].color, 1);
              this.graphics.beginPath();
              this.graphics.moveTo(this.lastPx, this.lastPy - this.pad.y);
              this.graphics.lineTo(px, py - this.pad.y);
              this.graphics.closePath();
              this.graphics.strokePath();
              this.pad.draw(this.graphics);

              this.pad.drawFrame(
                'atlas',
                'brush',
                px - 8,
                py - 8 - this.pad.y,
                1,
                this.hsv[i].color
              );

              if (this.drawTime % this.simplification === 0) {
                var offsx = px;
                var offsy = py - this.pad.y;

                this.points.push([Math.ceil(offsx / 2), Math.ceil(offsy / 2)]);
              }
            }

            this.lastPx = px;
            this.lastPy = py;

            i++;

            if (i === 360) {
              i = 0;
            }
          }
        }
      },
      this
    );
  }

  getFormattedTime() {
    var today = new Date();
    var y = today.getFullYear();
    // JavaScript months are 0-based.
    var m = today.getMonth() + 1;
    var d = today.getDate();
    var h = today.getHours();
    var mi = today.getMinutes();
    var s = today.getSeconds();
    return y + '-' + m + '-' + d + '-' + h + ':' + mi + ':' + s;
  }

  generateTextures(id, strokes) {
    var options = {
      bowing: 6,
      stroke: 'black',
      fill: undefined,
      fillStyle: undefined,
      strokeWidth: 3,
    };

    this.drawingCanvas.clear();

    this.rc.rectangle(0, 0, 256, 256, { roughness: 0 });

    var frames = [];
    for (var count = 0; count < 5; count++) {
      for (var i = 0; i < strokes.length; i++) {
        if (count === 0) {
          this.rc.curve(strokes[i], {
            bowing: 6,
            stroke: 'black',
            fill: 'white',
            fillStyle: 'solid',
            strokeWidth: 3,
          });
        } else {
          this.rc.curve(strokes[i], options);
        }
      }
      this.drawingCanvas.generateTexture(`${id}-${count}`);
      this.drawingCanvas.clear();
      if (count !== 0) {
        frames.push({ key: `${id}-${count}` });
      }
    }

    return frames;
  }

  createDrawing(id, strokes, frameRate = 8) {
    var container = this.add.container(
      this.pad.x + this.pad.width + 158,
      this.pad.y + 128
    );

    var frames = this.generateTextures(id, strokes);

    this.anims.create({
      key: `${id}-squiggle`,
      frames: frames,
      frameRate: frameRate,
      repeat: -1,
    });

    var base = this.add.sprite(0, 0, `${id}-${0}`);
    var spr = this.add.sprite(0, 0, `${id}-${0}`).play(`${id}-squiggle`);

    container.add(base);
    container.add(spr);

    container.setSize(base.width, base.height);
    //container.setScale(0);

    return container;
  }

  create() {
    Utils.load(() => {
      this.setup();
    });

    this.events.on('shutdown', this.shutdown, this);
    this.registry.events.on('changedata', this.registryChanged, this);
    this.registry.events.on('setdata', this.registryChanged, this);
  }

  registryChanged(parent, key, data) {}

  update() {}

  shutdown() {
    if (this.screenGroup) {
      this.screenGroup.destroy();
      this.screenGroup = null;
    }

    if (this.bg) {
      this.bg.destroy();
      this.bg = null;
    }

    this.hsv = null;
    this.points = null;

    if (this.pad) {
      this.pad.destroy();
      this.pad = null;
    }

    if (this.graphics) {
      this.graphics.destroy();
      this.graphics = null;
    }

    this.registry.events.off('changedata', this.registryChanged, this);
    this.registry.events.off('setdata', this.registryChanged, this);
    this.events.off('shutdown', this.shutdown, this);

    console.log('kill doodle maker');
  }
}
