import Utils from 'utils/utils';
import UIHelper from 'utils/uiHelper';

export default class GameUI extends Phaser.Scene {
  constructor() {
    super({
      key: 'gameUI',
      active: false,
    });
  }

  init(data) {
    this.initData = data;
  }

  create() {
    this.clickSnd = this.sound.add('click');

    this.sprGroup = this.add.group();

    this.exitButton = UIHelper.createSystemButton(
      this,
      10,
      10,
      'systembuttons30',
      'systembuttons31'
    );
    this.exitButton.setOrigin(0, 0);
    this.exitButton.alpha = 0;
    this.exitButton.on('pointerup', (pointer) => {
      this.registry.set('exit', true);
    });

    this.soundButton = this.add.sprite(70, 10, 'atlas', 'systembuttons0');
    this.soundButton.setOrigin(0, 0);
    this.soundButton.alpha = 0;
    this.soundButton.setInteractive();

    if (Utils.SavedSettings.micMuted) {
      this.soundButton.setFrame('systembuttons2');
    }

    this.soundButton.on('pointerup', () => this.soundClick());

    this.soundButton.on('pointerover', (event) => {
      if (Utils.SavedSettings.micMuted) {
        this.soundButton.setFrame('systembuttons3');
      } else {
        this.soundButton.setFrame('systembuttons1');
      }
    });

    this.soundButton.on('pointerout', (event) => {
      if (Utils.SavedSettings.micMuted) {
        this.soundButton.setFrame('systembuttons2');
      } else {
        this.soundButton.setFrame('systembuttons0');
      }
    });

    this.confirmButton = this.add.sprite(130, 10, 'atlas', 'systembuttons14');
    this.confirmButton.setOrigin(0, 0);
    this.confirmButton.alpha = 0;
    this.confirmButton.setInteractive();

    if (Utils.SavedSettings.bypassTextConfirmation) {
      this.confirmButton.setFrame('systembuttons16');
    }

    this.confirmButton.on('pointerup', () => this.textConfirmClick());

    this.confirmButton.on('pointerover', (event) => {
      if (Utils.SavedSettings.bypassTextConfirmation) {
        this.confirmButton.setFrame('systembuttons17');
      } else {
        this.confirmButton.setFrame('systembuttons15');
      }
    });

    this.confirmButton.on('pointerout', (event) => {
      if (Utils.SavedSettings.bypassTextConfirmation) {
        this.confirmButton.setFrame('systembuttons16');
      } else {
        this.confirmButton.setFrame('systembuttons14');
      }
    });

    this.clearButton = UIHelper.createSystemButton(
      this,
      190,
      10,
      'systembuttons18',
      'systembuttons19'
    );
    this.clearButton.setOrigin(0, 0);
    this.clearButton.alpha = 0;
    this.clearButton.on('pointerup', (pointer) => {
      this.registry.set('clearAll', true);
    });

    this.saveButton = UIHelper.createSystemButton(
      this,
      10,
      70,
      'systembuttons28',
      'systembuttons29'
    );
    this.saveButton.setOrigin(0, 0);
    this.saveButton.alpha = 0;
    this.saveButton.on('pointerup', (pointer) => {
      this.registry.set('exportPDF', true);
    });

    this.tweens.add({
      targets: [
        this.exitButton,
        this.soundButton,
        this.confirmButton,
        this.clearButton,
        this.saveButton,
      ],
      alpha: 1,
      ease: 'Linear',
      duration: 1000,
    });

    this.bgOverlay = this.add.image(0, 0, 'atlas', 'overlay');
    this.bgOverlay.setOrigin(0);
    this.bgOverlay.setDisplaySize(
      Utils.GlobalSettings.width,
      Utils.GlobalSettings.height
    );
    this.bgOverlay.alpha = 0;
    this.bgOverlay.visible = false;
    this.bgOverlay.setTint(0x000000);
    this.bgOverlay.setInteractive();

    if (this.initData) {
      this.changeThemeColor(this.initData.color);
    }

    this.events.on('shutdown', this.shutdown, this);
    this.registry.events.on('changedata', this.registryChanged, this);
    this.registry.events.on('setdata', this.registryChanged, this);
  }

  registryChanged(parent, key, data) {
    if (key === 'muted' && data) {
      Utils.SavedSettings.micMuted = true;
      Utils.save();
      this.soundButton.setFrame('systembuttons2');
    } else if (key === 'changeColor') {
      this.changeThemeColor(data);
    } else if (key === 'toggleUI') {
      this.toggleUI(data);
    }
  }

  changeThemeColor(color) {
    this.exitButton.setTint(color);
    this.soundButton.setTint(color);
    this.confirmButton.setTint(color);
    this.clearButton.setTint(color);
    this.saveButton.setTint(color);
  }

  toggleUI(visible) {
    this.exitButton.visible = visible;
    this.soundButton.visible = visible;
    this.confirmButton.visible = visible;
    this.clearButton.visible = visible;
    this.saveButton.visible = visible;
  }

  showBGOverlay() {
    this.bgOverlay.alpha = 0;
    this.bgOverlay.visible = true;
    this.tweens.add({
      targets: [this.bgOverlay],
      alpha: 0.5,
      ease: 'Linear',
      duration: 1000,
    });
  }

  hideBGOverlay() {
    this.bgOverlay.alpha = 0;
    this.bgOverlay.visible = false;
  }

  soundClick() {
    Utils.SavedSettings.micMuted = !Utils.SavedSettings.micMuted;
    Utils.save();
    if (!Utils.SavedSettings.micMuted) {
      if (!Utils.SavedSettings.muted) {
        this.clickSnd.play();
      }
      this.soundButton.setFrame('systembuttons0');

      this.registry.set('muted', false);
    } else {
      this.soundButton.setFrame('systembuttons2');
      this.registry.set('muted', true);
    }
  }

  textConfirmClick() {
    if (!Utils.SavedSettings.muted) {
      this.clickSnd.play();
    }
    Utils.SavedSettings.bypassTextConfirmation = !Utils.SavedSettings
      .bypassTextConfirmation;
    Utils.save();
    if (!Utils.SavedSettings.bypassTextConfirmation) {
      this.confirmButton.setFrame('systembuttons14');
    } else {
      this.confirmButton.setFrame('systembuttons16');
    }
  }

  shutdown() {
    if (this.sprGroup) {
      this.sprGroup.destroy();
      this.sprGroup = null;
    }

    if (this.soundButton) {
      this.soundButton.destroy();
      this.soundButton = null;
    }

    if (this.confirmButton) {
      this.confirmButton.destroy();
      this.confirmButton = null;
    }

    if (this.clickSnd) {
      this.clickSnd.destroy();
      this.clickSnd = null;
    }

    this.registry.events.off('changedata', this.registryChanged, this);
    this.registry.events.off('setdata', this.registryChanged, this);
    this.events.off('shutdown', this.shutdown, this);

    console.log('kill ui');
  }
}
