import Utils from 'utils/utils';
import Categories from 'utils/categories';

export default class Preloader extends Phaser.Scene {
  constructor() {
    super('preloader');
    this.progressBar = null;
    this.progressBarRectangle = null;
  }

  preload() {
    this.load.setCORS('Anonymous');

    this.load.image('menubg', 'assets/menubg.jpg');
    this.load.image('paperbg', 'assets/paperbg.jpg');
    this.load.image('storytitle', 'assets/storytitle.jpg');
    this.load.image('bg1', 'assets/bg1.jpg');
    this.load.image('bg2', 'assets/bg2.jpg');
    this.load.image('bg3', 'assets/bg3.jpg');
    this.load.image('bg4', 'assets/bg4.jpg');
    this.load.image('bg5', 'assets/bg5.jpg');
    this.load.image('dialog9patch1', 'assets/dialog9patch1.png');
    this.load.image('dialog9patch2', 'assets/dialog9patch2.png');

    this.load.multiatlas({
      key: 'atlas',
      atlasURL: 'assets/atlas-multi.json',
      path: 'assets/',
    });

    this.load.audio('gp', ['assets/audio/gp.ogg', 'assets/audio/gp.m4a']);
    this.load.audio('click', [
      'assets/audio/click.ogg',
      'assets/audio/click.m4a',
    ]);
    this.load.audio('bgm1', ['assets/audio/bgm1.ogg', 'assets/audio/bgm1.m4a']);

    this.load.bitmapFont(
      'sakkal',
      'assets/fonts/sakkal_0.png',
      'assets/fonts/sakkal.xml'
    );

    this.load.bitmapFont(
      'handwriting',
      'assets/fonts/handwriting_0.png',
      'assets/fonts/handwriting.xml'
    );

    this.load.json('text', 'assets/data/text.json');
    this.load.json('particles', 'assets/data/effects.json');

    Categories.AllCategoriesDictionary = {};
    for (var i = 0; i < Categories.AllCategories.length; i++) {
      var category = Categories.AllCategories[i];
      Categories.AllCategoriesDictionary[category] = category;

      //compound categories
      if (category.indexOf(' ') !== -1) {
        var parts = category.split(' ');
        if (parts.length > 0) {
          Categories.CompoundCategories[category] = parts;
        }
      }

      this.load.json(category, `assets/data/drawings/${category}.json`);
    }

    for (var key in Utils.SavedSettings.customDoodles) {
      Categories.AllCategoriesDictionary[key] = key;
    }

    this.load.plugin(
      'rexninepatchplugin',
      'plugins/rexninepatchplugin.min.js',
      true
    );

    this.load.plugin('rexcanvasplugin', 'plugins/rexcanvasplugin.min.js', true);

    this.load.on('progress', this.onLoadProgress, this);
    this.load.on('complete', this.onLoadComplete, this);
    this.createProgressBar();

    this.icon = this.add.image(
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height / 2,
      'logo'
    );
    this.icon.setOrigin(0.5, 0.5);
  }

  create() {
    Utils.GlobalSettings.bgm = this.sound.add('bgm1');
    Utils.GlobalSettings.text = this.cache.json.get('text');

    this.message = this.add.bitmapText(
      0,
      Utils.GlobalSettings.height - 350,
      'handwriting',
      'Tap To Play!',
      50
    );
    this.message.setOrigin(0, 0);

    this.message.x = Utils.GlobalSettings.width / 2 - this.message.width / 2;

    var storyId = Utils.getQueryVariable('id');
    if (storyId) {
      this.message.setText('Loading Story!');
      this.message.x = Utils.GlobalSettings.width / 2 - this.message.width / 2;
      Utils.getFromWeb(storyId, (data) => {
        console.log(data);
        this.message.setText('Tap To Play!');
        this.message.x =
          Utils.GlobalSettings.width / 2 - this.message.width / 2;

        this.input.on('pointerdown', () => {
          this.scene.start('storyviewer', data);
        });
      });
    } else {
      this.input.on('pointerdown', () => {
        this.transitionOut();
      });
    }

    this.events.on('shutdown', this.shutdown, this);
  }

  transitionOut(target = 'menu') {
    var tween = this.tweens.add({
      targets: [this.icon, this.message],
      alpha: 0,
      ease: 'Power1',
      duration: 1000,
      onComplete: () => {
        this.scene.start(target, { from: 'preloader' });
      },
    });
  }

  // extend:

  createProgressBar() {
    var main = this.cameras.main;
    this.progressBarRectangle = new Phaser.Geom.Rectangle(
      0,
      0,
      0.75 * main.width,
      20
    );
    Phaser.Geom.Rectangle.CenterOn(
      this.progressBarRectangle,
      0.5 * main.width,
      main.height - 150
    );
    this.progressBar = this.add.graphics();
  }

  onLoadComplete(loader) {
    //console.log('onLoadComplete', loader);
    this.progressBar.destroy();
  }

  onLoadProgress(progress) {
    var rect = this.progressBarRectangle;
    var color = 0xca59ff;
    this.progressBar
      .clear()
      .fillStyle(0x222222)
      .fillRect(rect.x, rect.y, rect.width, rect.height)
      .fillStyle(color)
      .fillRect(rect.x, rect.y, progress * rect.width, rect.height);
    //console.log('progress', progress);
  }

  update() {}

  shutdown() {
    if (this.icon) {
      this.icon.destroy();
      this.icon = null;
    }

    if (this.progressBarRectangle) {
      this.progressBarRectangle = null;
    }

    this.events.off('shutdown', this.shutdown, this);

    console.log('kill preloader');
  }
}
