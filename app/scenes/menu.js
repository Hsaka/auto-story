import Utils from 'utils/utils';
import UIHelper from 'utils/uiHelper';

export default class MenuScene extends Phaser.Scene {
  constructor() {
    super('menu');
  }

  init(data) {
    this.initData = data;
  }

  setup() {
    this.screenGroup = this.add.container(0, 0);

    this.bg = this.add.image(
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height / 2,
      'menubg'
    );
    this.bg.setOrigin(0.5, 0.5);
    this.bg.alpha = 0;
    this.screenGroup.add(this.bg);

    this.newStoryButton = UIHelper.createTintedButton(
      this,
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height / 2 - 150,
      'menubutton1',
      0xff0000
    );
    this.newStoryButton.alpha = 0;
    this.newStoryButton.on('pointerup', (pointer) => {
      Utils.SavedSettings.micMuted = false;
      Utils.SavedSettings.bypassTextConfirmation = false;
      Utils.GlobalSettings.story = {
        allDoodles: {},
        allDoodleNames: {},
        pages: [],
      };
      if (annyang) {
        annyang.start();
      }
      this.scene.start('main', { mode: 'new' });
    });

    this.continueButton = UIHelper.createTintedButton(
      this,
      Utils.GlobalSettings.width / 2,
      this.newStoryButton.y + 100,
      'menubutton3',
      0xff0000
    );
    this.continueButton.alpha = 0;
    this.continueButton.on('pointerup', (pointer) => {
      if (Utils.SavedSettings.lastStory) {
        if (annyang) {
          annyang.start();
        }
        this.scene.start('main', { mode: 'continue' });
      } else {
        UIHelper.showToast(
          this,
          this.toast,
          this.continueButton.x,
          this.continueButton.y - 50,
          'No story to continue...',
          500,
          250
        );
      }
    });

    this.doodleMakerButton = UIHelper.createTintedButton(
      this,
      Utils.GlobalSettings.width / 2,
      this.continueButton.y + 100,
      'menubutton2',
      0xff0000
    );
    this.doodleMakerButton.alpha = 0;
    this.doodleMakerButton.on('pointerup', (pointer) => {
      this.scene.start('doodlemaker', { from: 'menu' });
      //this.scene.start('storyviewer', Utils.SavedSettings.myStories[0]);
    });

    this.versionTxt = this.add.bitmapText(
      5,
      5,
      'sakkal',
      'v: ' + Utils.GlobalSettings.version,
      25
    );
    this.versionTxt.setTint(0x000000);

    this.creditIndex = 0;
    this.creditText = ['Code: Hsaka', 'Graphics: freepik [www.freepik.com]'];

    this.credits = this.add.bitmapText(
      10,
      Utils.GlobalSettings.height,
      'sakkal',
      this.creditText[0],
      50
    );
    this.credits.alpha = 0;

    this.toast = UIHelper.createToast(this);

    this.tweens.add({
      targets: this.credits,
      alpha: 1,
      y: Utils.GlobalSettings.height - 50,
      ease: 'Sine.easeOut',
      duration: 2500,
      delay: 100,
      yoyo: true,
      loop: -1,
      onLoop: () => {
        this.creditIndex++;
        if (this.creditIndex >= this.creditText.length) {
          this.creditIndex = 0;
        }
        this.credits.setText(this.creditText[this.creditIndex]);
      },
    });

    this.tweens.add({
      targets: [
        this.bg,
        this.newStoryButton,
        this.continueButton,
        this.doodleMakerButton,
      ],
      alpha: 1,
      ease: 'Linear.easeOut',
      duration: 1000,
      delay: this.tweens.stagger(500),
    });

    if (this.scene.isActive('gameUI') && !this.scene.isSleeping('gameUI')) {
      this.scene.sleep('gameUI');
    }
  }

  create() {
    this.setup();

    this.events.on('shutdown', this.shutdown, this);
    this.registry.events.on('changedata', this.registryChanged, this);
    this.registry.events.on('setdata', this.registryChanged, this);
  }

  registryChanged(parent, key, data) {
    if (key === 'muted') {
    }
  }

  update(time, delta) {}

  shutdown() {
    if (this.screenGroup) {
      this.screenGroup.destroy();
      this.screenGroup = null;
    }

    if (this.bg) {
      this.bg.destroy();
      this.bg = null;
    }

    if (this.newStoryButton) {
      this.newStoryButton.destroy();
      this.newStoryButton = null;
    }

    if (this.doodleMakerButton) {
      this.doodleMakerButton.destroy();
      this.doodleMakerButton = null;
    }

    this.registry.events.off('changedata', this.registryChanged, this);
    this.registry.events.off('setdata', this.registryChanged, this);
    this.events.off('shutdown', this.shutdown, this);

    console.log('kill menu');
  }
}
