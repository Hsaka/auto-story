import Utils from 'utils/utils';
import UIHelper from 'utils/uiHelper';
import Categories from 'utils/categories';

export default class MainScene extends Phaser.Scene {
  constructor() {
    super('main');
  }

  init(data) {
    if (data.mode === 'new') {
      this.storyData = null;
    } else if (data.mode === 'continue') {
      this.storyData = Utils.SavedSettings.lastStory;
    }
  }

  setup() {
    // if (!Utils.GlobalSettings.bgm.isPlaying) {
    //   Utils.GlobalSettings.bgm.setVolume(0.2);
    //   Utils.GlobalSettings.bgm.setLoop(true);
    //   Utils.GlobalSettings.bgm.play();
    // }

    // if (Utils.SavedSettings.muted) {
    //   Utils.GlobalSettings.bgm.pause();
    // }

    this.selectedDoodleId = undefined;
    this.doodles = {};
    this.doodleNameDict = {};
    this.currentPageDoodles = {};
    this.story = '';
    this.storyName = '';
    this.authorName = '';
    this.linkName = '';
    this.pageIndex = 0;
    this.bgFrame = 'bg1';
    this.defaultBackdropData = undefined;
    this.currentBackdropData = undefined;
    this.undoBuffer = [];

    this.screenGroup = this.add.container(0, 0);

    this.doodleGroup = this.add.container(0, 0);

    this.bg = this.add.sprite(0, 0, this.bgFrame);
    this.bg.setOrigin(0, 0);
    this.bg.alpha = 0;
    this.screenGroup.add(this.bg);

    this.bg.setInteractive();
    this.bg.on('pointerdown', (pointer) => {
      this.selectedDoodleId = undefined;
      this.selectedFrame.visible = false;
      pointer.event.stopPropagation();
    });

    this.drawingCanvas = this.add.rexCanvas(0, 0, 256, 256).setOrigin(0);
    this.drawingCanvas.setAlpha(0);
    this.rc = rough.canvas(this.drawingCanvas.getCanvas());

    this.selectedFrame = this.add.image(0, 0, 'atlas', 'frame');
    this.selectedFrame.visible = false;
    this.selectedFrame.alpha = 0.5;

    this.trashCan = this.add.image(
      Utils.GlobalSettings.width - 120,
      20,
      'atlas',
      'trash'
    );
    this.trashCan.setOrigin(0).setInteractive();
    this.trashCan.visible = false;
    this.trashCan.input.dropZone = true;

    this.input.on('dragenter', (pointer, gameObject, dropZone) => {
      this.trashCan.setTint(0xff0000);
    });

    this.input.on('dragleave', (pointer, gameObject, dropZone) => {
      this.trashCan.clearTint();
    });

    this.input.on('drop', (pointer, gameObject, dropZone) => {
      if (gameObject) {
        // delete this.doodles[gameObject._id];
        // if (gameObject._adjectives && gameObject._adjectives.name) {
        //   delete this.doodleNameDict[gameObject._adjectives.name];
        // }

        // gameObject.destroy();

        delete this.currentPageDoodles[gameObject._id];
        this.selectedFrame.visible = false;
        this.selectedDoodleId = undefined;
        gameObject.visible = false;
      }
      this.trashCan.clearTint();
      this.trashCan.visible = false;
    });

    this.eraseButton = UIHelper.createSystemButton(
      this,
      Utils.GlobalSettings.width / 2 + 320,
      10,
      'systembuttons24',
      'systembuttons25'
    );
    this.eraseButton.setOrigin(0, 0);
    this.eraseButton.alpha = 0;
    this.eraseButton.visible = false;
    this.eraseButton.on('pointerup', (pointer) => {
      this.eraseButton.alpha = 0;
      this.eraseButton.visible = false;
      this.undoButton.alpha = 0;
      this.undoButton.visible = false;
      this.story = '';
      this.storyText.setText(this.story);
      this.undoBuffer = [];
    });

    this.undoButton = UIHelper.createSystemButton(
      this,
      Utils.GlobalSettings.width / 2 + 320,
      70,
      'systembuttons26',
      'systembuttons27'
    );
    this.undoButton.setOrigin(0, 0);
    this.undoButton.alpha = 0;
    this.undoButton.visible = false;
    this.undoButton.on('pointerup', (pointer) => {
      if (this.undoBuffer.length > 0) {
        this.undoBuffer.pop();
        if (this.undoBuffer.length > 0) {
          this.story = this.undoBuffer[this.undoBuffer.length - 1];
          this.storyText.setText(this.story);
        } else {
          this.story = '';
          this.storyText.setText(this.story);
          this.eraseButton.alpha = 0;
          this.eraseButton.visible = false;
          this.undoButton.alpha = 0;
          this.undoButton.visible = false;
        }
      } else {
        this.story = '';
        this.storyText.setText(this.story);
        this.eraseButton.alpha = 0;
        this.eraseButton.visible = false;
        this.undoButton.alpha = 0;
        this.undoButton.visible = false;
      }
    });

    this.nextPageButton = UIHelper.createSystemButton(
      this,
      Utils.GlobalSettings.width - 60,
      Utils.GlobalSettings.height - 60,
      'systembuttons20',
      'systembuttons21'
    );
    this.nextPageButton.setOrigin(0, 0);
    this.nextPageButton.alpha = 0;
    this.nextPageButton.on('pointerup', (pointer) => {
      this.nextPage();
    });

    this.prevPageButton = UIHelper.createSystemButton(
      this,
      10,
      Utils.GlobalSettings.height - 60,
      'systembuttons22',
      'systembuttons23'
    );
    this.prevPageButton.setOrigin(0, 0);
    this.prevPageButton.alpha = 0;
    this.prevPageButton.visible = false;
    this.prevPageButton.on('pointerup', (pointer) => {
      this.prevPage();
    });

    this.tweens.add({
      targets: [this.bg, this.nextPageButton],
      alpha: 1,
      ease: 'Linear',
      duration: 1000,
    });

    this.storyText = this.add.bitmapText(
      Utils.GlobalSettings.width / 2,
      25,
      'handwriting',
      this.story,
      55
    );
    this.storyText.setMaxWidth(500);
    this.storyText.setTint(0x000000).setOrigin(0.5, 0).setCenterAlign();

    this.pageText = this.add.bitmapText(
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height - 40,
      'handwriting',
      `Page ${this.pageIndex + 1}`,
      30
    );
    this.pageText.setTint(0x000000).setOrigin(0.5, 0).setCenterAlign();

    this.setupTextInputPanel();
    this.setupTitleInputPanel();
    this.setupLinkPanel();

    this.titleBg = this.add.sprite(0, 0, 'storytitle');
    this.titleBg.setOrigin(0, 0);
    this.titleBg.visible = false;

    this.titleText = this.add.bitmapText(
      Utils.GlobalSettings.width / 2,
      100,
      'handwriting',
      '',
      100
    );
    this.titleText.setMaxWidth(700);
    this.titleText.visible = false;
    this.titleText.setTint(0x000000).setOrigin(0.5, 0).setCenterAlign();

    this.authorText = this.add.bitmapText(
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height - 50,
      'handwriting',
      '',
      35
    );
    this.authorText.setMaxWidth(700);
    this.authorText.visible = false;
    this.authorText.setTint(0x000000).setOrigin(0.5, 0).setCenterAlign();

    if (annyang) {
      annyang.addCallback('result', (phrases) => {
        if (!Utils.SavedSettings.micMuted) {
          console.log('I think the user said: ', phrases[0]);
          console.log(
            'But then again, it could be any of the following: ',
            phrases
          );

          if (Utils.SavedSettings.bypassTextConfirmation) {
            this.parseStatement(phrases[0]);
            this.addToStory(phrases[0]);
          } else {
            this.showTextInputPanel(phrases);
          }
        } else {
          console.log('muted');
        }
      });
    }

    this.defaultBackdropData = Categories.GetBackdrop('day');

    if (this.scene.isSleeping('gameUI')) {
      this.scene.wake('gameUI', this.defaultBackdropData);
      this.scene.bringToTop('gameUI');
    } else if (!this.scene.isActive('gameUI')) {
      this.scene.launch('gameUI', this.defaultBackdropData);
      this.scene.bringToTop('gameUI');
    }

    this.switchBackdrop(this.defaultBackdropData);

    if (this.storyData && Object.keys(this.storyData).length > 0) {
      this.setupStory();
      this.loadPage(this.pageIndex);
    }
  }

  setupStory() {
    for (var id in this.storyData.allDoodles) {
      var doodleData = this.storyData.allDoodles[id];

      var doodle = this.createDrawing(
        id,
        doodleData.drawing,
        null,
        doodleData.adjectives,
        8,
        false
      );
      doodle.visible = false;
    }

    this.currentPageDoodles = {};
    this.doodleNameDict = this.storyData.allDoodleNames;
    this.storyName = this.storyData.title;
    this.authorName = this.storyData.author;
    Utils.GlobalSettings.story = this.storyData;
  }

  getFormattedTime() {
    var today = new Date();
    var y = today.getFullYear();
    // JavaScript months are 0-based.
    var m = today.getMonth() + 1;
    var d = today.getDate();
    var h = today.getHours();
    var mi = today.getMinutes();
    var s = today.getSeconds();
    return y + '-' + m + '-' + d + '-' + h + '-' + mi + '-' + s;
  }

  showTitle(callback) {
    this.titleBg.visible = true;
    this.titleBg.alpha = 0;
    this.titleText.visible = true;
    this.titleText.alpha = 0;
    this.titleText.setText(this.storyName);

    this.authorText.visible = true;
    this.authorText.alpha = 0;
    this.authorText.setText(`A story by ${this.authorName}`);

    this.registry.set('toggleUI', false);
    if (annyang) {
      annyang.pause();
    }
    Utils.SavedSettings.micMuted = true;

    this.tweens.add({
      targets: [this.titleBg, this.titleText, this.authorText],
      alpha: 1,
      ease: 'Linear',
      duration: 1,
      onComplete: () => {
        this.exportScreenshot((image) => {
          Utils.GlobalSettings.story.titleBg = image.src;
          if (callback && typeof callback === 'function') {
            callback();
          }
        });
      },
    });
  }

  hideTitle() {
    this.titleBg.visible = false;
    this.titleText.visible = false;
    this.authorText.visible = false;

    this.registry.set('toggleUI', true);
    if (annyang) {
      annyang.resume();
    }
    Utils.SavedSettings.micMuted = false;
  }

  exportToPDF() {
    this.hideTextInputPanel();

    var images = [];

    images.push({
      image: Utils.GlobalSettings.story.titleBg,
      width: 750,
    });

    this.exportScreenshot((image) => {
      this.savePage(this.pageIndex, image);

      for (var i = 0; i < Utils.GlobalSettings.story.pages.length; i++) {
        images.push({
          image: Utils.GlobalSettings.story.pages[i].screenshot,
          width: 750,
        });
      }

      pdfMake
        .createPdf({
          pageSize: 'LETTER',
          pageOrientation: 'landscape',
          pageMargins: [20, 30, 20, 30],
          content: images,
        })
        .download(
          `${this.storyName.replace(
            /[^a-z0-9]/gi,
            '_'
          )}_${this.getFormattedTime()}.pdf`
        );

      Utils.SavedSettings.lastStory = Utils.GlobalSettings.story;
      Utils.save();

      this.showLinkPanel();
      this.linkLabel.visible = false;
      this.linkTextField.textBoxField.visible = false;
      this.linkTextField.textboxDialog.visible = false;
      this.linkLoadingLabel.visible = true;
      this.linkPanelCloseButton.visible = false;

      if (Utils.GlobalSettings.story.id) {
        Utils.putToWeb(
          Utils.GlobalSettings.story.id,
          Utils.SavedSettings.lastStory,
          (id) => {
            this.linkLabel.visible = true;
            this.linkTextField.textBoxField.visible = true;
            this.linkTextField.textBoxField.setText(
              `${window.location.href}?id=${id}`
            );
            this.linkTextField.textboxDialog.visible = true;
            this.linkLoadingLabel.visible = false;
            this.linkPanelCloseButton.visible = true;
          }
        );
      } else {
        Utils.postToWeb(Utils.SavedSettings.lastStory, (id) => {
          this.linkLabel.visible = true;
          this.linkTextField.textBoxField.visible = true;
          this.linkTextField.textBoxField.setText(
            `${window.location.href}?id=${id}`
          );
          this.linkTextField.textboxDialog.visible = true;
          this.linkLoadingLabel.visible = false;
          this.linkPanelCloseButton.visible = true;
        });
      }
    });
  }

  hideAllUI() {
    for (var i = 0; i < window.game.scene.scenes.length; i++) {
      var scene = window.game.scene.scenes[i].scene;
      if (scene.key === 'gameUI') {
        scene.setVisible(false);
      }
    }

    this.nextPageButton.alpha = 0;
    this.prevPageButton.alpha = 0;
    this.trashCan.alpha = 0;
    this.eraseButton.alpha = 0;
    this.undoButton.alpha = 0;
    this.selectedFrame.alpha = 0;
  }

  showAllUI() {
    for (var i = 0; i < window.game.scene.scenes.length; i++) {
      var scene = window.game.scene.scenes[i].scene;
      if (scene.key === 'gameUI') {
        scene.setVisible(true);
      }
    }

    this.nextPageButton.alpha = 1;
    this.prevPageButton.alpha = 1;
    this.trashCan.alpha = 1;
    this.eraseButton.alpha = 1;
    this.undoButton.alpha = 1;
    this.selectedFrame.alpha = 0.5;
  }

  exportScreenshot(callback) {
    this.hideAllUI();

    window.game.renderer.snapshot((image) => {
      image.style.width = Utils.GlobalSettings.width + 'px';
      image.style.height = Utils.GlobalSettings.height + 'px';

      this.showAllUI();

      callback(image);
    });
  }

  savePage(pageIndex, image) {
    var doodleProps = {};

    for (var id in this.doodles) {
      var doodle = this.doodles[id];

      Utils.GlobalSettings.story.allDoodles[id] = {
        id: doodle._id,
        adjectives: doodle._adjectives,
        category: doodle._category,
        options: doodle._options,
        drawing: doodle._drawing,
      };

      if (this.currentPageDoodles[doodle._id]) {
        doodleProps[doodle._id] = {
          x: doodle.x,
          y: doodle.y,
          adjectives: JSON.parse(JSON.stringify(doodle._adjectives)),
        };
      }
    }

    Utils.GlobalSettings.story.allDoodleNames = this.doodleNameDict;

    var pageData = {
      storyText: this.story,
      doodles: doodleProps,
      bg: this.currentBackdropData,
      screenshot: image.src,
    };

    if (pageIndex >= Utils.GlobalSettings.story.pages.length) {
      Utils.GlobalSettings.story.pages.push(pageData);
    } else {
      Utils.GlobalSettings.story.pages[pageIndex] = pageData;
    }
  }

  nextPage() {
    this.hideTextInputPanel();
    this.exportScreenshot((image) => {
      this.savePage(this.pageIndex, image);
      this.clearAll();

      this.pageIndex++;

      this.prevPageButton.visible = true;
      this.tweens.add({
        targets: [this.prevPageButton],
        alpha: 1,
        ease: 'Linear',
        duration: 1000,
      });

      this.loadPage(this.pageIndex);
    });
  }

  prevPage() {
    this.hideTextInputPanel();
    this.exportScreenshot((image) => {
      this.savePage(this.pageIndex, image);
      this.clearAll();

      this.pageIndex--;
      if (this.pageIndex <= 0) {
        this.prevPageButton.visible = false;
        this.pageIndex = 0;
      }

      this.loadPage(this.pageIndex);
    });
  }

  loadPage(index) {
    this.hideTextInputPanel();

    this.pageText.setText(`Page ${index + 1}`);

    if (index < Utils.GlobalSettings.story.pages.length) {
      var page = Utils.GlobalSettings.story.pages[index];
      if (page) {
        this.story = page.storyText;
        this.storyText.setText(this.story);
        if (this.story.length > 0) {
          this.eraseButton.alpha = 1;
          this.eraseButton.visible = true;
        }
        this.switchBackdrop(page.bg);

        for (var id in page.doodles) {
          var drawing = this.doodles[id];
          if (drawing) {
            var doodle = page.doodles[id];
            drawing.x = doodle.x;
            drawing.y = doodle.y;
            drawing.visible = true;

            this.currentPageDoodles[id] = id;
            this.applyAdjectives(drawing, doodle.adjectives);
          }
        }
      }
    }
  }

  clearAll() {
    this.story = '';
    this.storyText.setText(this.story);
    this.eraseButton.alpha = 0;
    this.eraseButton.visible = false;
    this.undoButton.alpha = 0;
    this.undoButton.visible = false;
    this.undoBuffer = [];
    this.switchBackdrop(this.defaultBackdropData);

    for (var key in this.currentPageDoodles) {
      this.doodles[key].visible = false;
    }

    this.selectedFrame.visible = false;
    this.selectedDoodleId = undefined;
    this.currentPageDoodles = {};
  }

  setupTitleInputPanel() {
    this.titlePanelBgOverlay = this.add.image(0, 0, 'atlas', 'overlay');
    this.titlePanelBgOverlay.setOrigin(0);
    this.titlePanelBgOverlay.setDisplaySize(
      Utils.GlobalSettings.width,
      Utils.GlobalSettings.height
    );
    this.titlePanelBgOverlay.alpha = 0.9;
    this.titlePanelBgOverlay.visible = false;
    this.titlePanelBgOverlay.setTint(0x000000);
    this.titlePanelBgOverlay.setInteractive();

    this.titlePanelBg = UIHelper.createDialog(
      this,
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height / 2,
      900,
      300,
      'dialog9patch2',
      15
    );
    this.titlePanelBg.alpha = 0.6;
    this.titlePanelBg.visible = false;

    this.titleLabel = this.add.bitmapText(
      this.titlePanelBg.x - this.titlePanelBg.width / 2 + 10,
      this.titlePanelBg.y - this.titlePanelBg.height / 2 + 10,
      'handwriting',
      "What's the name of your story?",
      50
    );
    this.titleLabel.setTint(0x000000);
    this.titleLabel.visible = false;

    this.titleTextField = UIHelper.createTextField(
      this,
      this.titlePanelBg.x - 25,
      this.titleLabel.y + 260,
      800,
      400,
      this.storyName
    );
    this.titleTextField.textBoxField.visible = false;
    this.titleTextField.textboxDialog.visible = false;

    this.authorLabel = this.add.bitmapText(
      this.titlePanelBg.x - this.titlePanelBg.width / 2 + 10,
      this.titlePanelBg.y - this.titlePanelBg.height / 2 + 150,
      'handwriting',
      'Who is the author of this story?',
      50
    );
    this.authorLabel.setTint(0x000000);
    this.authorLabel.visible = false;

    this.authorTextField = UIHelper.createTextField(
      this,
      this.titlePanelBg.x - 25,
      this.authorLabel.y + 260,
      800,
      400,
      this.authorName
    );
    this.authorTextField.textBoxField.visible = false;
    this.authorTextField.textboxDialog.visible = false;

    this.titlePanelCloseButton = UIHelper.createSystemButton(
      this,
      this.titlePanelBg.x + this.titlePanelBg.width / 2 - 5,
      this.titlePanelBg.y - this.titlePanelBg.height / 2 + 5,
      'systembuttons10',
      'systembuttons11'
    );
    this.titlePanelCloseButton.visible = false;
    this.titlePanelCloseButton.on('pointerup', (pointer) => {
      pointer.event.stopPropagation();
      this.hideTitleInputPanel();
    });

    this.titlePanelConfirmButton = UIHelper.createSystemButton(
      this,
      this.authorTextField.textboxDialog.x +
        this.authorTextField.textboxDialog.width / 2 +
        30,
      this.authorTextField.textboxDialog.y -
        this.authorTextField.textboxDialog.height / 2 +
        25,
      'systembuttons12',
      'systembuttons13'
    );
    this.titlePanelConfirmButton.visible = false;
    this.titlePanelConfirmButton.on('pointerup', (pointer) => {
      var text = this.titleTextField.textBoxField.getElement('text').getText();
      var author = this.authorTextField.textBoxField
        .getElement('text')
        .getText();
      if (text.length > 0 && author.length > 0) {
        this.storyName = text;
        this.authorName = author;
        Utils.GlobalSettings.story.title = this.storyName;
        Utils.GlobalSettings.story.author = this.authorName;
        this.hideTitleInputPanel();
        this.showTitle(() => {
          this.hideTitle();
          this.exportToPDF();
        });
      }
    });
  }

  showTitleInputPanel() {
    this.registry.set('toggleUI', false);
    this.nextPageButton.alpha = 0;
    this.prevPageButton.alpha = 0;
    this.trashCan.alpha = 0;
    this.eraseButton.alpha = 0;
    this.undoButton.alpha = 0;
    this.selectedFrame.alpha = 0;
    this.titlePanelBgOverlay.visible = true;
    this.titlePanelBg.visible = true;
    this.titleLabel.visible = true;
    this.titleTextField.textBoxField.visible = true;
    this.titleTextField.textboxDialog.visible = true;
    this.authorLabel.visible = true;
    this.authorTextField.textBoxField.visible = true;
    this.authorTextField.textboxDialog.visible = true;
    this.titlePanelCloseButton.visible = true;
    this.titlePanelConfirmButton.visible = true;
    this.titleTextField.textBoxField.setText(this.storyName);
    this.authorTextField.textBoxField.setText(this.authorName);
  }

  hideTitleInputPanel() {
    this.registry.set('toggleUI', true);
    this.nextPageButton.alpha = 1;
    this.prevPageButton.alpha = 1;
    this.trashCan.alpha = 1;
    this.eraseButton.alpha = 1;
    this.undoButton.alpha = 1;
    this.selectedFrame.alpha = 0.5;
    this.titlePanelBgOverlay.visible = false;
    this.titlePanelBg.visible = false;
    this.titleLabel.visible = false;
    this.titleTextField.textBoxField.visible = false;
    this.titleTextField.textboxDialog.visible = false;
    this.authorLabel.visible = false;
    this.authorTextField.textBoxField.visible = false;
    this.authorTextField.textboxDialog.visible = false;
    this.titlePanelCloseButton.visible = false;
    this.titlePanelConfirmButton.visible = false;
  }

  setupLinkPanel() {
    this.linkPanelBgOverlay = this.add.image(0, 0, 'atlas', 'overlay');
    this.linkPanelBgOverlay.setOrigin(0);
    this.linkPanelBgOverlay.setDisplaySize(
      Utils.GlobalSettings.width,
      Utils.GlobalSettings.height
    );
    this.linkPanelBgOverlay.alpha = 0.9;
    this.linkPanelBgOverlay.visible = false;
    this.linkPanelBgOverlay.setTint(0x000000);
    this.linkPanelBgOverlay.setInteractive();

    this.linkPanelBg = UIHelper.createDialog(
      this,
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height / 2,
      900,
      200,
      'dialog9patch2',
      15
    );
    this.linkPanelBg.alpha = 0.6;
    this.linkPanelBg.visible = false;

    this.linkLabel = this.add.bitmapText(
      this.linkPanelBg.x - this.linkPanelBg.width / 2 + 10,
      this.linkPanelBg.y - this.linkPanelBg.height / 2 + 10,
      'handwriting',
      "Here's the link to your story:",
      50
    );
    this.linkLabel.setTint(0x000000);
    this.linkLabel.visible = false;

    this.linkTextField = UIHelper.createTextField(
      this,
      this.linkPanelBg.x - 25,
      this.linkLabel.y + 260,
      800,
      400,
      this.linkName
    );
    this.linkTextField.textBoxField.visible = false;
    this.linkTextField.textboxDialog.visible = false;

    this.linkLoadingLabel = this.add.bitmapText(
      this.linkPanelBg.x,
      this.linkPanelBg.y,
      'handwriting',
      'Saving Story... Please Wait',
      50
    );
    this.linkLoadingLabel.setTint(0x000000).setOrigin(0.5, 0).setCenterAlign();
    this.linkLoadingLabel.visible = false;

    this.linkPanelCloseButton = UIHelper.createSystemButton(
      this,
      this.linkPanelBg.x + this.linkPanelBg.width / 2 - 5,
      this.linkPanelBg.y - this.linkPanelBg.height / 2 + 5,
      'systembuttons10',
      'systembuttons11'
    );
    this.linkPanelCloseButton.visible = false;
    this.linkPanelCloseButton.on('pointerup', (pointer) => {
      pointer.event.stopPropagation();
      this.hideLinkPanel();
    });
  }

  showLinkPanel() {
    this.registry.set('toggleUI', false);
    this.nextPageButton.alpha = 0;
    this.prevPageButton.alpha = 0;
    this.trashCan.alpha = 0;
    this.eraseButton.alpha = 0;
    this.undoButton.alpha = 0;
    this.selectedFrame.alpha = 0;
    this.linkPanelBgOverlay.visible = true;
    this.linkPanelBg.visible = true;
    this.linkLabel.visible = true;
    this.linkLoadingLabel.visible = true;
    this.linkTextField.textBoxField.visible = true;
    this.linkTextField.textboxDialog.visible = true;
    this.linkPanelCloseButton.visible = true;
    this.linkPanelCloseButton.visible = true;
    this.linkTextField.textBoxField.setText(this.linkName);
  }

  hideLinkPanel() {
    this.registry.set('toggleUI', true);
    this.nextPageButton.alpha = 1;
    this.prevPageButton.alpha = 1;
    this.trashCan.alpha = 1;
    this.eraseButton.alpha = 1;
    this.undoButton.alpha = 1;
    this.selectedFrame.alpha = 0.5;
    this.linkPanelBgOverlay.visible = false;
    this.linkPanelBg.visible = false;
    this.linkLabel.visible = false;
    this.linkLoadingLabel.visible = false;
    this.linkTextField.textBoxField.visible = false;
    this.linkTextField.textboxDialog.visible = false;
    this.linkPanelCloseButton.visible = false;
  }

  setupTextInputPanel() {
    this.textPanelBg = UIHelper.createDialog(
      this,
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height / 2,
      900,
      400,
      'dialog9patch2',
      15
    );
    this.textPanelBg.alpha = 0.6;

    this.didYouSayLabel = this.add.bitmapText(
      this.textPanelBg.x - this.textPanelBg.width / 2 + 10,
      this.textPanelBg.y - this.textPanelBg.height / 2 + 10,
      'handwriting',
      'Did you say:',
      50
    );
    this.didYouSayLabel.setTint(0x000000);

    this.textFields = [];
    this.textFieldSelectButtons = [];

    for (var i = 0; i < 4; i++) {
      var textField = UIHelper.createTextField(
        this,
        this.textPanelBg.x - 25,
        this.didYouSayLabel.y + 260 + i * 80,
        800,
        400,
        ''
      );
      textField.textBoxField.visible = false;
      textField.textboxDialog.visible = false;

      this.textFields.push(textField);

      var selectButton = UIHelper.createSystemButton(
        this,
        textField.textboxDialog.x + textField.textboxDialog.width / 2 + 30,
        textField.textboxDialog.y - textField.textboxDialog.height / 2 + 25,
        'systembuttons12',
        'systembuttons13'
      );
      selectButton.visible = false;
      selectButton._index = i;
      selectButton.on(
        'pointerup',
        function (selectButton, pointer) {
          pointer.event.stopPropagation();
          this.inputTextSelected(selectButton._index);
        }.bind(this, selectButton)
      );
      this.textFieldSelectButtons.push(selectButton);
    }

    this.textPanelCloseButton = UIHelper.createSystemButton(
      this,
      this.textPanelBg.x + this.textPanelBg.width / 2 - 5,
      this.textPanelBg.y - this.textPanelBg.height / 2 + 5,
      'systembuttons10',
      'systembuttons11'
    );
    this.textPanelCloseButton.on('pointerup', (pointer) => {
      pointer.event.stopPropagation();
      this.hideTextInputPanel();
    });

    this.textPanelBg.visible = false;
    this.didYouSayLabel.visible = false;
    this.textPanelCloseButton.visible = false;
  }

  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  endsWithPunctuation(string) {
    return /[.,:!?]/.test(string.charAt(string.length - 1));
  }

  addToStory(text) {
    var formattedText = text.trim();
    formattedText = this.capitalizeFirstLetter(formattedText);

    if (!this.endsWithPunctuation(formattedText)) {
      formattedText += '.';
    }

    this.story += formattedText + ' ';

    this.storyText.setText(this.story);

    this.undoBuffer.push(this.story);

    this.eraseButton.alpha = 0;
    this.eraseButton.visible = true;
    this.undoButton.alpha = 0;
    this.undoButton.visible = true;
    this.tweens.add({
      targets: [this.eraseButton, this.undoButton],
      alpha: 1,
      ease: 'Linear',
      duration: 1000,
    });
  }

  inputTextSelected(index) {
    var textField = this.textFields[index];
    var text = textField.textBoxField.getElement('text').getText();
    this.parseStatement(text);

    this.addToStory(text);

    this.hideTextInputPanel();
  }

  showTextInputPanel(phrases) {
    this.textPanelBg.visible = true;
    this.didYouSayLabel.visible = true;
    this.textPanelCloseButton.visible = true;

    for (var i = 0; i < this.textFields.length; i++) {
      var textField = this.textFields[i];
      textField.textBoxField.visible = false;
      textField.textboxDialog.visible = false;

      var selectButton = this.textFieldSelectButtons[i];
      selectButton.visible = false;
    }

    for (var i = 0; i < phrases.length; i++) {
      if (i < this.textFields.length) {
        var textField = this.textFields[i];
        textField.textBoxField.visible = true;
        textField.textboxDialog.visible = true;
        textField.textBoxField.getElement('text').setText(phrases[i]);

        var selectButton = this.textFieldSelectButtons[i];
        selectButton.visible = true;
      }
    }
  }

  hideTextInputPanel() {
    this.textPanelBg.visible = false;
    this.didYouSayLabel.visible = false;
    this.textPanelCloseButton.visible = false;

    for (var i = 0; i < this.textFields.length; i++) {
      var textField = this.textFields[i];
      textField.textBoxField.visible = false;
      textField.textboxDialog.visible = false;

      var selectButton = this.textFieldSelectButtons[i];
      selectButton.visible = false;
    }
  }

  getCategoryFromDictionary(word) {
    if (pluralize.isPlural(word)) {
      if (Categories.AllCategoriesDictionary[word]) {
        return {
          word: word,
          isPlural: false,
        };
      } else if (Categories.AllCategoriesDictionary[pluralize.singular(word)]) {
        return {
          word: pluralize.singular(word),
          isPlural: false,
        };
      }
    } else if (Categories.AllCategoriesDictionary[word]) {
      return {
        word: word,
        isPlural: false,
      };
    }
  }

  parseStatement(statement) {
    if (statement && statement.length > 0) {
      var used = {};
      var adjectives = {
        stroke: undefined,
        fill: undefined,
        fillStyle: undefined,
        size: undefined,
        name: undefined,
      };

      var doodle = undefined;
      if (this.selectedDoodleId && this.doodles[this.selectedDoodleId]) {
        doodle = this.doodles[this.selectedDoodleId];
        doodle.visible = true;
        this.currentPageDoodles[doodle._id] = doodle._id;
      }

      var parts = statement.split(' ');
      if (parts && parts.length > 0) {
        for (var j = 0; j < parts.length; j++) {
          var part = parts[j].toLowerCase().trim();
          var prev = this.getPrevWord(parts, j);
          var next = this.getNextWord(parts, j);

          if (this.doodleNameDict[part]) {
            if (this.doodles[this.doodleNameDict[part]]) {
              doodle = this.doodles[this.doodleNameDict[part]];
              doodle.visible = true;
              this.currentPageDoodles[doodle._id] = doodle._id;
            }
          }

          this.parseBackdrop(part);

          var adjectivesChanged = this.parseAdjectives(part, adjectives);

          if (doodle) {
            if (adjectivesChanged) {
              this.applyAdjectives(doodle, adjectives);
            }

            var nameConfig = this.parseName(part, parts, j);
            if (nameConfig) {
              adjectives.name = nameConfig.name;
              j = nameConfig.index;
              this.addOrUpdateNameTag(doodle, adjectives);
              continue;
            }
          }

          if (!this.selectedDoodleId) {
            //Parse Words ---
            //parse compound words
            var isCompound = false;

            if (next) {
              var compound = part + ' ' + next;

              var compoundWord = this.getCategoryFromDictionary(compound);

              if (
                compoundWord &&
                Categories.AllCategoriesDictionary[compoundWord.word] &&
                !used[compoundWord.word]
              ) {
                used[compoundWord.word] = true;
                isCompound = true;
                j++;
                doodle = this.generateDrawing(
                  compoundWord.word,
                  null,
                  adjectives
                );
              }
            }

            if (!isCompound) {
              var singleWord = this.getCategoryFromDictionary(part);

              if (
                singleWord &&
                Categories.AllCategoriesDictionary[singleWord.word] &&
                !used[singleWord.word]
              ) {
                used[singleWord.word] = true;
                doodle = this.generateDrawing(
                  singleWord.word,
                  null,
                  adjectives
                );
              }
            }
            //--- Parse Words
          }
        }
      }
    }
  }

  getNextWord(parts, index) {
    var next = undefined;
    if (index < parts.length - 1) {
      next = parts[index + 1].toLowerCase().trim();
    }
    return next;
  }

  getPrevWord(parts, index) {
    var prev = undefined;
    if (index > 0) {
      prev = parts[index - 1].toLowerCase().trim();
    }
    return prev;
  }

  switchBackdrop(backdropData) {
    if (backdropData) {
      this.currentBackdropData = backdropData;
      this.bg.setTexture(backdropData.frame);
      this.bgFrame = backdropData.frame;
      this.storyText.setTint(backdropData.color);
      this.registry.set('changeColor', backdropData.color);
      this.eraseButton.setTint(backdropData.color);
      this.undoButton.setTint(backdropData.color);
      this.nextPageButton.setTint(backdropData.color);
      this.prevPageButton.setTint(backdropData.color);
    }
  }

  parseBackdrop(part) {
    var backdropData = Categories.GetBackdrop(part);
    this.switchBackdrop(backdropData);
  }

  parseName(part, parts, index) {
    var prev = this.getPrevWord(parts, index);
    var next = this.getNextWord(parts, index);
    var name = undefined;
    switch (part) {
      case 'name':
        if (next) {
          if (next === 'was' || next === 'is' || next === 'of') {
            name = this.getNextWord(parts, index + 1);
            index += 2;
          }
        }
        break;

      case 'named':
        if (next) {
          if (next === 'him' || next === 'her' || next === 'it') {
            name = this.getNextWord(parts, index + 1);
            index += 2;
          } else {
            name = next;
            index += 1;
          }
        }
        break;

      case 'called':
        if (next) {
          if (next === 'him' || next === 'her' || next === 'it') {
            name = this.getNextWord(parts, index + 1);
            index += 2;
          } else {
            name = next;
            index += 1;
          }
        }
        break;

      case 'known':
        if (next) {
          if (next === 'as' || next === 'by') {
            name = this.getNextWord(parts, index + 1);
            index += 2;
          }
        }
        break;
    }

    if (name) {
      return {
        name: name.trim(),
        index: index,
      };
    }
  }

  parseAdjectives(part, adjectives) {
    var changed = false;
    if (Categories.Colors[part]) {
      //adjectives.stroke = part;
      adjectives.fill = part;
      changed = true;
    }

    if (Categories.Sizes[part]) {
      adjectives.size = Categories.Sizes[part];
      changed = true;
    }
    return changed;
  }

  generateTextures(id, strokes, options, adjectives) {
    adjectives.stroke = adjectives.stroke ? adjectives.stroke : 'black';
    adjectives.fill = adjectives.fill ? adjectives.fill : undefined;
    adjectives.fillStyle = adjectives.fillStyle
      ? adjectives.fillStyle
      : Phaser.Math.RND.pick(Categories.FillStyles);
    adjectives.size = adjectives.size ? adjectives.size : 'medium';

    if (!options) {
      options = {
        bowing: 6,
        stroke: adjectives.stroke,
        fill: adjectives.fill,
        fillStyle: adjectives.fillStyle,
        strokeWidth: 3,
      };
    }

    this.drawingCanvas.clear();

    var frames = [];
    for (var count = 0; count < 5; count++) {
      for (var i = 0; i < strokes.drawing.length; i++) {
        if (count === 0) {
          this.rc.curve(strokes.drawing[i], {
            bowing: 6,
            stroke: adjectives.stroke,
            fill: 'white',
            fillStyle: 'solid',
            strokeWidth: 3,
          });
        } else {
          this.rc.curve(strokes.drawing[i], options);
        }
      }
      this.drawingCanvas.generateTexture(`${id}-${count}`);
      this.drawingCanvas.clear();
      if (count !== 0) {
        frames.push({ key: `${id}-${count}` });
      }
    }

    return frames;
  }

  setDrawingScale(container, adjectives) {
    if (container._size !== adjectives.size) {
      var newScale = 0;
      switch (adjectives.size) {
        case 'tiny':
          newScale = 0.25;
          break;
        case 'small':
          newScale = 0.5;
          break;
        case 'medium':
          newScale = 0.75;
          break;
        case 'large':
          newScale = 1.2;
          break;
      }

      container._size = adjectives.size;

      this.tweens.add({
        targets: [container],
        scaleX: { from: container.scaleX, to: newScale },
        scaleY: { from: container.scaleY, to: newScale },
        ease: 'Back.easeOut',
        duration: 1000,
      });

      if (
        this.selectedFrame.visible &&
        this.selectedDoodleId === container._id
      ) {
        this.tweens.add({
          targets: [this.selectedFrame],
          scaleX: { from: this.selectedFrame.scaleX, to: newScale },
          scaleY: { from: this.selectedFrame.scaleY, to: newScale },
          ease: 'Back.easeOut',
          duration: 1000,
        });
      }

      this.tweens.add({
        targets: [container._nameTag],
        scaleX: { from: container.scaleX, to: 1.5 },
        scaleY: { from: container.scaleY, to: 1.5 },
        ease: 'Back.easeOut',
        duration: 1000,
      });
    }
  }

  addOrUpdateNameTag(container, adjectives) {
    if (adjectives.name) {
      if (!container._nameTag) {
        var nameText = this.add.bitmapText(
          0,
          -180,
          'handwriting',
          adjectives.name,
          60
        );
        nameText.setTint(0x000000);
        nameText.setOrigin(0.5).setCenterAlign();
        container.add(nameText);

        container._nameTag = nameText;
      } else {
        container._nameTag.setText(adjectives.name);
      }

      container._adjectives.name = adjectives.name;

      this.doodleNameDict[adjectives.name] = container._id;

      this.tweens.add({
        targets: container._nameTag,
        alpha: { from: 0, to: 1 },
        y: { from: -150, to: -180 },
        ease: 'Sine.easeOut',
        duration: 1000,
      });
    }
  }

  applyAdjectives(container, adjectives) {
    if (container) {
      container._adjectives.fill = adjectives.fill
        ? adjectives.fill
        : container._adjectives.fill;
      container._adjectives.size = adjectives.size
        ? adjectives.size
        : container._adjectives.size;
      container._adjectives.name = adjectives.name
        ? adjectives.name
        : container._adjectives.name;

      if (adjectives.fill && container._fill !== adjectives.fill) {
        this.generateTextures(
          container._id,
          container._drawing,
          null,
          container._adjectives
        );
        container._fill = adjectives.fill;
      }

      this.setDrawingScale(container, container._adjectives);

      this.addOrUpdateNameTag(container, container._adjectives);
    }
  }

  createDrawing(id, strokes, options, adjectives, frameRate = 8, spawn = true) {
    var container = this.add.container(
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height / 2
    );

    this.doodleGroup.add(container);

    var frames = this.generateTextures(id, strokes, options, adjectives);

    this.anims.create({
      key: `${id}-squiggle`,
      frames: frames,
      frameRate: frameRate,
      repeat: -1,
    });

    var base = this.add.sprite(0, 0, `${id}-${0}`);
    var spr = this.add.sprite(0, 0, `${id}-${0}`).play(`${id}-squiggle`);

    container.add(base);
    container.add(spr);

    container.setSize(base.width, base.height);
    container.setScale(0);

    container._baseSprite = base;
    container._animSprite = spr;
    container._drawing = strokes;
    container._size = 'tiny';
    container._fill = adjectives.fill;
    container._category = strokes.word;
    container._options = options;
    container._adjectives = adjectives;
    container._id = id;

    this.addOrUpdateNameTag(container, adjectives);

    this.setDrawingScale(container, adjectives);

    this.doodles[id] = container;
    this.currentPageDoodles[id] = id;

    container.setInteractive();
    this.input.setDraggable(container);

    container.on('pointerover', function () {
      base.setTint(0x44ff44);
    });

    container.on('pointerout', function () {
      base.clearTint();
    });

    container.on('pointerup', () => {
      if (container.visible) {
        this.selectedDoodleId = id;

        this.selectedFrame.setScale(container.scaleX, container.scaleY);
        this.selectedFrame.x = container.x;
        this.selectedFrame.y = container.y;
        this.selectedFrame.visible = true;
      }
    });

    this.input.on('drag', (pointer, gameObject, dragX, dragY) => {
      gameObject.x = dragX;
      gameObject.y = dragY;
    });

    this.input.on('dragstart', (pointer, gameObject) => {
      gameObject._baseSprite.setTint(0x44ffff);
      this.selectedFrame.visible = false;
      this.trashCan.visible = true;
    });

    this.input.on('dragend', (pointer, gameObject) => {
      gameObject._baseSprite.clearTint();
      this.selectedDoodleId = undefined;
      this.selectedFrame.visible = false;
      this.trashCan.visible = false;
    });

    if (spawn) {
      this.tweens.add({
        targets: [container],
        x: {
          from: container.x,
          to: Phaser.Math.RND.integerInRange(
            Utils.GlobalSettings.width / 2 - 250,
            Utils.GlobalSettings.width / 2 + 250
          ),
        },
        y: {
          from: container.y,
          to: Phaser.Math.RND.integerInRange(
            Utils.GlobalSettings.height / 2 - 250,
            Utils.GlobalSettings.height / 2 + 250
          ),
        },
        ease: 'Sine.easeOut',
        duration: 1000,
      });
    }

    return container;
  }

  generateDrawing(category, options, adjectives, frameRate = 8) {
    var id = category + '-' + Phaser.Math.RND.uuid();
    var drawings = this.cache.json.get(category);

    //try loading custom doodle
    if (!drawings) {
      drawings = Utils.SavedSettings.customDoodles[category];
    }

    if (drawings && drawings.length > 0) {
      var strokes = Phaser.Math.RND.pick(drawings);

      return this.createDrawing(id, strokes, options, adjectives);
    }
  }

  create() {
    this.setup();

    this.events.on('shutdown', this.shutdown, this);
    this.registry.events.on('changedata', this.registryChanged, this);
    this.registry.events.on('setdata', this.registryChanged, this);
  }

  registryChanged(parent, key, data) {
    switch (key) {
      case 'exit':
        if (data) {
          this.scene.start('menu', { from: 'main' });
        }
        break;
      case 'clearAll':
        if (data) {
          this.clearAll();
        }
        break;
      case 'exportPDF':
        if (data) {
          this.showTitleInputPanel();
        }
        break;
      case 'muted':
        if (data) {
          if (annyang) {
            annyang.pause();
          }
        } else {
          if (annyang) {
            annyang.resume();
          }
        }
        break;
    }
  }

  shutdown() {
    if (this.screenGroup) {
      this.screenGroup.destroy();
      this.screenGroup = null;
    }

    if (this.bg) {
      this.bg.destroy();
      this.bg = null;
    }

    if (annyang) {
      annyang.abort();
      annyang.removeCallback();
    }

    this.registry.events.off('changedata', this.registryChanged, this);
    this.registry.events.off('setdata', this.registryChanged, this);
    this.events.off('shutdown', this.shutdown, this);

    console.log('kill main');
  }
}
