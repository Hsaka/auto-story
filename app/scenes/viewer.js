import Utils from 'utils/utils';
import UIHelper from 'utils/uiHelper';
import Categories from 'utils/categories';

export default class StoryViewer extends Phaser.Scene {
  constructor() {
    super('storyviewer');
  }

  init(data) {
    this.storyData = data;
  }

  setup() {
    // if (!Utils.GlobalSettings.bgm.isPlaying) {
    //   Utils.GlobalSettings.bgm.setVolume(0.2);
    //   Utils.GlobalSettings.bgm.setLoop(true);
    //   Utils.GlobalSettings.bgm.play();
    // }

    // if (Utils.SavedSettings.muted) {
    //   Utils.GlobalSettings.bgm.pause();
    // }

    this.doodles = {};
    this.doodleNameDict = {};
    this.currentPageDoodles = {};
    this.story = '';
    this.storyName = this.storyData.title;
    this.pageIndex = 0;
    this.bgFrame = 'bg1';
    this.defaultBackdropData = undefined;
    this.currentBackdropData = undefined;

    this.screenGroup = this.add.container(0, 0);

    this.doodleGroup = this.add.container(0, 0);

    this.bg = this.add.sprite(0, 0, this.bgFrame);
    this.bg.setOrigin(0, 0);
    this.bg.alpha = 0;
    this.screenGroup.add(this.bg);

    this.bg.setInteractive();
    this.bg.on('pointerdown', (pointer) => {
      this.selectedDoodleId = undefined;
      this.selectedFrame.visible = false;
      pointer.event.stopPropagation();
    });

    this.drawingCanvas = this.add.rexCanvas(0, 0, 256, 256).setOrigin(0);
    this.drawingCanvas.setAlpha(0);
    this.rc = rough.canvas(this.drawingCanvas.getCanvas());

    this.selectedFrame = this.add.image(0, 0, 'atlas', 'frame');
    this.selectedFrame.visible = false;
    this.selectedFrame.alpha = 0.5;

    this.exitButton = UIHelper.createSystemButton(
      this,
      10,
      10,
      'systembuttons30',
      'systembuttons31'
    );
    this.exitButton.setOrigin(0, 0);
    this.exitButton.alpha = 0;
    this.exitButton.on('pointerup', (pointer) => {
      this.scene.start('menu', { from: 'main' });
    });

    this.saveButton = UIHelper.createSystemButton(
      this,
      70,
      10,
      'systembuttons28',
      'systembuttons29'
    );
    this.saveButton.setOrigin(0, 0);
    this.saveButton.alpha = 0;
    this.saveButton.on('pointerup', (pointer) => {
      this.exportToPDF();
    });

    this.nextPageButton = UIHelper.createSystemButton(
      this,
      Utils.GlobalSettings.width - 60,
      Utils.GlobalSettings.height - 60,
      'systembuttons20',
      'systembuttons21'
    );
    this.nextPageButton.setOrigin(0, 0);
    this.nextPageButton.alpha = 0;
    this.nextPageButton.on('pointerup', (pointer) => {
      this.nextPage();
    });

    this.prevPageButton = UIHelper.createSystemButton(
      this,
      10,
      Utils.GlobalSettings.height - 60,
      'systembuttons22',
      'systembuttons23'
    );
    this.prevPageButton.setOrigin(0, 0);
    this.prevPageButton.alpha = 0;
    this.prevPageButton.visible = false;
    this.prevPageButton.on('pointerup', (pointer) => {
      this.prevPage();
    });

    this.storyText = this.add.bitmapText(
      Utils.GlobalSettings.width / 2,
      25,
      'handwriting',
      this.story,
      55
    );
    this.storyText.setMaxWidth(500);
    this.storyText.setTint(0x000000).setOrigin(0.5, 0).setCenterAlign();

    this.pageText = this.add.bitmapText(
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height - 40,
      'handwriting',
      `Page ${this.pageIndex + 1}`,
      30
    );
    this.pageText.setTint(0x000000).setOrigin(0.5, 0).setCenterAlign();

    this.titleBg = this.add.sprite(0, 0, 'storytitle');
    this.titleBg.setOrigin(0, 0);
    this.titleBg.setInteractive();
    this.titleBg.on('pointerdown', (pointer) => {
      this.titleBg.disableInteractive();
      this.tweens.add({
        targets: [this.titleBg, this.titleText, this.authorText],
        alpha: 0,
        ease: 'Linear',
        duration: 500,
        onComplete: () => {
          this.titleBg.visible = false;
          this.titleText.visible = false;
          this.authorText.visible = false;
        },
      });
    });

    this.titleText = this.add.bitmapText(
      Utils.GlobalSettings.width / 2,
      100,
      'handwriting',
      this.storyData.title,
      100
    );
    this.titleText.setMaxWidth(700);
    this.titleText.alpha = 0;
    this.titleText.setTint(0x000000).setOrigin(0.5, 0).setCenterAlign();

    this.authorText = this.add.bitmapText(
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height - 50,
      'handwriting',
      `A story by ${this.storyData.author}`,
      35
    );
    this.authorText.setMaxWidth(700);
    this.authorText.alpha = 0;
    this.authorText.setTint(0x000000).setOrigin(0.5, 0).setCenterAlign();

    this.tweens.add({
      targets: [
        this.bg,
        this.exitButton,
        this.saveButton,
        this.nextPageButton,
        this.titleText,
        this.authorText,
      ],
      alpha: 1,
      ease: 'Linear',
      duration: 1000,
    });

    if (this.scene.isActive('gameUI') && !this.scene.isSleeping('gameUI')) {
      this.scene.sleep('gameUI');
    }

    this.setupStory();
    this.loadPage(this.pageIndex);
    if (this.pageIndex >= this.storyData.pages.length - 1) {
      this.nextPageButton.visible = false;
    }
  }

  setupStory() {
    for (var id in this.storyData.allDoodles) {
      var doodleData = this.storyData.allDoodles[id];

      var doodle = this.createDrawing(
        id,
        doodleData.drawing,
        null,
        doodleData.adjectives
      );
      doodle.visible = false;
    }
  }

  getFormattedTime() {
    var today = new Date();
    var y = today.getFullYear();
    // JavaScript months are 0-based.
    var m = today.getMonth() + 1;
    var d = today.getDate();
    var h = today.getHours();
    var mi = today.getMinutes();
    var s = today.getSeconds();
    return y + '-' + m + '-' + d + '-' + h + '-' + mi + '-' + s;
  }

  exportToPDF() {
    var images = [];

    images.push({
      image: this.storyData.titleBg,
      width: 750,
    });

    for (var i = 0; i < this.storyData.pages.length; i++) {
      images.push({
        image: this.storyData.pages[i].screenshot,
        width: 750,
      });
    }

    if (this.pageIndex === this.storyData.pages.length) {
      this.exportScreenshot((image) => {
        images.push({
          image: image.src,
          width: 750,
        });

        pdfMake
          .createPdf({
            pageSize: 'LETTER',
            pageOrientation: 'landscape',
            pageMargins: [20, 30, 20, 30],
            content: images,
          })
          .download(
            `${this.storyName.replace(
              /[^a-z0-9]/gi,
              '_'
            )}_${this.getFormattedTime()}.pdf`
          );
      });
    } else {
      pdfMake
        .createPdf({
          pageSize: 'LETTER',
          pageOrientation: 'landscape',
          pageMargins: [20, 30, 20, 30],
          content: images,
        })
        .download(
          `${this.storyName.replace(
            /[^a-z0-9]/gi,
            '_'
          )}_${this.getFormattedTime()}.pdf`
        );
    }
  }

  exportScreenshot(callback) {
    var activeScenes = [];
    for (var i = 0; i < window.game.scene.scenes.length; i++) {
      var scene = window.game.scene.scenes[i].scene;
      if (scene.key === 'gameUI') {
        activeScenes.push(scene);
        scene.setVisible(false);
      }
    }

    this.saveButton.alpha = 0;
    this.exitButton.alpha = 0;
    this.nextPageButton.alpha = 0;
    this.prevPageButton.alpha = 0;

    window.game.renderer.snapshot((image) => {
      image.style.width = Utils.GlobalSettings.width + 'px';
      image.style.height = Utils.GlobalSettings.height + 'px';

      for (var i = 0; i < activeScenes.length; i++) {
        var scene = activeScenes[i];
        scene.setVisible(true);
      }

      this.saveButton.alpha = 1;
      this.exitButton.alpha = 1;
      this.nextPageButton.alpha = 1;
      this.prevPageButton.alpha = 1;

      callback(image);
    });
  }

  savePage(pageIndex, image) {
    var doodleProps = {};

    for (var id in this.doodles) {
      var doodle = this.doodles[id];

      this.storyData.allDoodles[id] = {
        id: doodle._id,
        adjectives: doodle._adjectives,
        category: doodle._category,
        options: doodle._options,
        drawing: doodle._drawing,
      };

      if (this.currentPageDoodles[doodle._id]) {
        doodleProps[doodle._id] = {
          x: doodle.x,
          y: doodle.y,
          adjectives: JSON.parse(JSON.stringify(doodle._adjectives)),
        };
      }
    }

    this.storyData.allDoodleNames = this.doodleNameDict;

    var pageData = {
      storyText: this.story,
      doodles: doodleProps,
      bg: this.currentBackdropData,
      screenshot: image.src,
    };

    if (pageIndex >= this.storyData.pages.length) {
      this.storyData.pages.push(pageData);
    } else {
      this.storyData.pages[pageIndex] = pageData;
    }
  }

  nextPage() {
    this.exportScreenshot((image) => {
      this.savePage(this.pageIndex, image);
      this.clearAll();

      this.pageIndex++;

      if (this.pageIndex >= this.storyData.pages.length - 1) {
        this.nextPageButton.visible = false;
        this.pageIndex = this.storyData.pages.length - 1;
      }

      this.prevPageButton.visible = true;
      this.tweens.add({
        targets: [this.prevPageButton],
        alpha: 1,
        ease: 'Linear',
        duration: 1000,
      });

      this.loadPage(this.pageIndex);
    });
  }

  prevPage() {
    this.exportScreenshot((image) => {
      this.savePage(this.pageIndex, image);
      this.clearAll();

      this.pageIndex--;
      if (this.pageIndex <= 0) {
        this.prevPageButton.visible = false;
        this.pageIndex = 0;
      }

      this.nextPageButton.visible = true;

      this.loadPage(this.pageIndex);
    });
  }

  loadPage(index) {
    this.pageText.setText(`Page ${index + 1}`);

    if (index < this.storyData.pages.length) {
      var page = this.storyData.pages[index];
      console.log(page);
      if (page) {
        this.story = page.storyText;
        this.storyText.setText(this.story);
        this.switchBackdrop(page.bg);

        for (var id in page.doodles) {
          var drawing = this.doodles[id];
          if (drawing) {
            var doodle = page.doodles[id];
            drawing.x = doodle.x;
            drawing.y = doodle.y;
            drawing.visible = true;

            this.currentPageDoodles[id] = id;
            this.applyAdjectives(drawing, doodle.adjectives);
          }
        }
      }
    }
  }

  clearAll() {
    this.story = '';
    this.storyText.setText(this.story);
    this.switchBackdrop(this.defaultBackdropData);

    for (var key in this.currentPageDoodles) {
      this.doodles[key].visible = false;
    }

    this.currentPageDoodles = {};
  }

  switchBackdrop(backdropData) {
    if (backdropData) {
      this.currentBackdropData = backdropData;
      this.bg.setTexture(backdropData.frame);
      this.bgFrame = backdropData.frame;
      this.storyText.setTint(backdropData.color);
      this.saveButton.setTint(backdropData.color);
      this.exitButton.setTint(backdropData.color);
      this.nextPageButton.setTint(backdropData.color);
      this.prevPageButton.setTint(backdropData.color);
    }
  }

  generateTextures(id, strokes, options, adjectives) {
    adjectives.stroke = adjectives.stroke ? adjectives.stroke : 'black';
    adjectives.fill = adjectives.fill ? adjectives.fill : undefined;
    adjectives.fillStyle = adjectives.fillStyle
      ? adjectives.fillStyle
      : Phaser.Math.RND.pick(Categories.FillStyles);
    adjectives.size = adjectives.size ? adjectives.size : 'medium';

    if (!options) {
      options = {
        bowing: 6,
        stroke: adjectives.stroke,
        fill: adjectives.fill,
        fillStyle: adjectives.fillStyle,
        strokeWidth: 3,
      };
    }

    this.drawingCanvas.clear();

    var frames = [];
    for (var count = 0; count < 5; count++) {
      for (var i = 0; i < strokes.drawing.length; i++) {
        if (count === 0) {
          this.rc.curve(strokes.drawing[i], {
            bowing: 6,
            stroke: adjectives.stroke,
            fill: 'white',
            fillStyle: 'solid',
            strokeWidth: 3,
          });
        } else {
          this.rc.curve(strokes.drawing[i], options);
        }
      }
      this.drawingCanvas.generateTexture(`${id}-${count}`);
      this.drawingCanvas.clear();
      if (count !== 0) {
        frames.push({ key: `${id}-${count}` });
      }
    }

    return frames;
  }

  setDrawingScale(container, adjectives) {
    if (container._size !== adjectives.size) {
      var newScale = 0;
      switch (adjectives.size) {
        case 'tiny':
          newScale = 0.25;
          break;
        case 'small':
          newScale = 0.5;
          break;
        case 'medium':
          newScale = 0.75;
          break;
        case 'large':
          newScale = 1.2;
          break;
      }

      container._size = adjectives.size;

      this.tweens.add({
        targets: [container],
        scaleX: { from: container.scaleX, to: newScale },
        scaleY: { from: container.scaleY, to: newScale },
        ease: 'Back.easeOut',
        duration: 1000,
      });

      this.tweens.add({
        targets: [container._nameTag],
        scaleX: { from: container.scaleX, to: 1.5 },
        scaleY: { from: container.scaleY, to: 1.5 },
        ease: 'Back.easeOut',
        duration: 1000,
      });
    }
  }

  addOrUpdateNameTag(container, adjectives) {
    if (adjectives.name) {
      console.log(adjectives);
      if (!container._nameTag) {
        var nameText = this.add.bitmapText(
          0,
          -180,
          'handwriting',
          adjectives.name,
          60
        );
        nameText.setTint(0x000000);
        nameText.setOrigin(0.5).setCenterAlign();
        container.add(nameText);

        container._nameTag = nameText;
      } else {
        container._nameTag.setText(adjectives.name);
      }

      container._adjectives.name = adjectives.name;

      this.doodleNameDict[adjectives.name] = container._id;

      this.tweens.add({
        targets: container._nameTag,
        alpha: { from: 0, to: 1 },
        y: { from: -150, to: -180 },
        ease: 'Sine.easeOut',
        duration: 1000,
      });
    }
  }

  applyAdjectives(container, adjectives) {
    if (container) {
      container._adjectives.fill = adjectives.fill
        ? adjectives.fill
        : container._adjectives.fill;
      container._adjectives.size = adjectives.size
        ? adjectives.size
        : container._adjectives.size;
      container._adjectives.name = adjectives.name
        ? adjectives.name
        : container._adjectives.name;

      if (adjectives.fill && container._fill !== adjectives.fill) {
        this.generateTextures(
          container._id,
          container._drawing,
          null,
          container._adjectives
        );
        container._fill = adjectives.fill;
      }

      this.setDrawingScale(container, container._adjectives);

      this.addOrUpdateNameTag(container, container._adjectives);
    }
  }

  createDrawing(id, strokes, options, adjectives, frameRate = 8) {
    var container = this.add.container(
      Utils.GlobalSettings.width / 2,
      Utils.GlobalSettings.height / 2
    );

    this.doodleGroup.add(container);

    var frames = this.generateTextures(id, strokes, options, adjectives);

    this.anims.create({
      key: `${id}-squiggle`,
      frames: frames,
      frameRate: frameRate,
      repeat: -1,
    });

    var base = this.add.sprite(0, 0, `${id}-${0}`);
    var spr = this.add.sprite(0, 0, `${id}-${0}`).play(`${id}-squiggle`);

    container.add(base);
    container.add(spr);

    container.setSize(base.width, base.height);
    container.setScale(0);

    container._baseSprite = base;
    container._animSprite = spr;
    container._drawing = strokes;
    container._size = 'tiny';
    container._fill = adjectives.fill;
    container._category = strokes.word;
    container._options = options;
    container._adjectives = adjectives;
    container._id = id;

    this.addOrUpdateNameTag(container, adjectives);

    this.setDrawingScale(container, adjectives);

    this.doodles[id] = container;

    container.setInteractive();
    this.input.setDraggable(container);

    container.on('pointerover', function () {
      base.setTint(0x44ff44);
    });

    container.on('pointerout', function () {
      base.clearTint();
    });

    this.input.on('drag', (pointer, gameObject, dragX, dragY) => {
      gameObject.x = dragX;
      gameObject.y = dragY;
    });

    this.input.on('dragstart', (pointer, gameObject) => {
      gameObject._baseSprite.setTint(0x44ffff);
    });

    this.input.on('dragend', (pointer, gameObject) => {
      gameObject._baseSprite.clearTint();
    });

    return container;
  }

  create() {
    this.setup();

    this.events.on('shutdown', this.shutdown, this);
    this.registry.events.on('changedata', this.registryChanged, this);
    this.registry.events.on('setdata', this.registryChanged, this);
  }

  registryChanged(parent, key, data) {}

  shutdown() {
    if (this.screenGroup) {
      this.screenGroup.destroy();
      this.screenGroup = null;
    }

    if (this.bg) {
      this.bg.destroy();
      this.bg = null;
    }

    this.registry.events.off('changedata', this.registryChanged, this);
    this.registry.events.off('setdata', this.registryChanged, this);
    this.events.off('shutdown', this.shutdown, this);

    console.log('kill viewer');
  }
}
